import org.junit.jupiter.api.Test;
import twaran.Rectangle;

import static org.junit.jupiter.api.Assertions.assertEquals;

public class RectangleTest {

    @Test
    void shouldReturnAreaIfLengthAndBreadthArePositive() {

        Rectangle rectangle = new Rectangle(5, 10);

        final int actualAreaOfRectangle = rectangle.area();

        assertEquals(50, actualAreaOfRectangle, "Area of twaran.Rectangle Test");
    }
}
